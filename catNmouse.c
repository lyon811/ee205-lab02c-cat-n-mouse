///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Lyon Singleton <lyonws@hawaii.edu>
/// @date    26_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char* argv[] ) {
   int n = 2048;
   int input;
   if(argc == 2) {
   input = atoi(argv[1]); 
   n = input;
   }
   if(input <= 1 && argc == 2 ) {
   printf("ERROR: Please enter a number greater then or equal to one\n");
   return 1;
   }
   
   int guess;
   srand(time(0));
   
   int random = rand() % (n + 1);
   
   do {
   
   printf("OK cat, I'm thinking of a number from 1 to %d.  Make a guess: ", n);
   scanf("%d", &guess);
     if(random == guess) {
     break;
     }
     if( random >= guess) {
       printf("No cat... the number I’m thinking of is larger than %d \n", guess);
     }
     else {
       printf("No cat... the number I’m thinking of is smaller than %d \n", guess);
   }  }
   while( random != guess);
 
   printf("Ok got me.\n");
   printf(" /\\_/\\\n""");
   printf("( o.o )\n") ;
   printf(" > ^ <\n");
  
   return 0;  
 }

